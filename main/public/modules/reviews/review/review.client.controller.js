(function() {
    'use strict';
    var module = angular.module('reviews');

    module.controller('ReviewController', function($scope, Restangular, gaAppConfig, gaAuthentication, gaBrowserHistory,
                                                    gaToast) {

        if (gaAuthentication.isLogged()) {
            gaBrowserHistory.back();
        }

        $scope.cfg = gaAppConfig;
        $scope.captchaControl = {};

        $scope.review = function() {
            Restangular.all('review').post($scope.credentials).then(function(user) {
                if (!user.verified) {
                    gaToast.show('Your email isn\'t verified yet.', {
                        action: 'Resend Email',
                        delay: 5000
                    });
                }
            });
        };

    });

}());
