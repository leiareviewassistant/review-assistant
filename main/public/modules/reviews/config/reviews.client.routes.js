(function() {
    'use strict';

    var module = angular.module('reviews');
    module.config(function($stateProvider) {
        $stateProvider
            .state('review', {
                url: '/review/create',
                controller: 'ReviewController',
                templateUrl: '/p/modules/reviews/review/review.client.view.html'
            });
    });
}());
