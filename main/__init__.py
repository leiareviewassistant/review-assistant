# coding: utf-8
"""
hackathon-emag
~~~~~~~~

Easiest way to create a review using Leia a Sensible Artificial Intelligent Assistant

https://www.bitbucket.org/leiareviewassistant/review-assistant
http://hackathon-emag.appspot.com

by Hackerstreet Boys.
License MIT, see LICENSE for more details.

"""

__version__ = '1'
from .main import API
from .main import app
