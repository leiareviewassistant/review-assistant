# coding: utf-8
# pylint: disable=wildcard-import
"""
Provides logic for authenticating users
"""
from .auth import *
