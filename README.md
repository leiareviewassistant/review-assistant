#Leia Review Assistant
#####Easiest way to create a review using Leia a Sensible Artificial Intelligent Assistant

This full stack uses following technologies:
* Google App Engine
* Python Flask
* AngularJS
* Angular Material
* Gulp, Bower, npm

######You can see live demo here: https://hackathon-emag.appspot.com/

##What's implemented? @TODO
* one
* two


#### Deploy

    gulp build
    appcfg.py update main

